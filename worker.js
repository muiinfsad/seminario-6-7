var cart = []
let time = 1000

/////////////////////////////////

const zmq = require('zeromq')
let req = zmq.socket('req')
req.identity = 'Worker-' + process.pid
req.connect('tcp://localhost:3001')
console.log(req.identity + ' waiting for requests')

req.send(['xxx', '', "free"])

req.on('message', (client, sep, message) => {
    message = message.toString()
    console.log('Processing->' + message)

    if (message.split(':')[0] === 'add') {
        add(message.split(':')[1], client)
    }

    if (message.split(':')[0] === 'remove') {
        remove(message.split(':')[1], client)
    }

    if (message.split(':')[0] === 'list') {
        list(client)
    }

    if (message.split(':')[0] === 'exit') {
        exit(client)
    }

})

function add(item, client) {
    setTimeout(() => {
        console.log("adding " + item)
        cart.push(item)
        req.send([client, '', 'added'])
        console.log('added');
    }, time)
}

function remove(item, client) {
    setTimeout(() => {
        console.log("removing " + item)
        cart.splice(cart.indexOf(item),1)
        req.send([client, '', 'removed'])
        console.log('removed');
    }, time)
}

function list(client) {
    setTimeout(() => {
        console.log("listing items")
        if (cart.length > 0)
            req.send([client, '', cart.toString()])
        else
            req.send([client, '', 'empty'])
            console.log('listed');
    }, time)
}

function exit(client) {
    setTimeout(() => {
        console.log("exit")
        cart = []
        req.send([client, '', 'exit'])
        console.log('exited');
    }, time)
}