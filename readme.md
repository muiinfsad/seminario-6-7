
# Seminario 6-7

## Pregunta enunciado:
¿Podría usarse el broker implementado con ZeroMQ y los carritos con REST y conectarse de
alguna manera.? ¿Proporciona esta solución alguna ventaja?
Sí podrían con algún servicio de intermediario que reciba los mensajes de zmq y los convierta en peticiones REST. No proporcionaría ninguna ventaja.

Para un correcto funcionamiento de la aplicación, será necesario ejecutar antes el comando ```npm install```

Este repositorio se compone de 3 ficheros : **broker.js**, **worker.js** y **client.js**.

El broker debe iniciarse antes que cualquier worker o client y estos últimos se pueden ejecutar en cualquier orden después. Las conexiones están puestas en localhost con diferentes puertos: entre el broker y los clientes se usa el puerto 3000 y entre el broker y los workers se usa el puerto 3001.

## broker.js

Es el intermediario entre los clientes y los workers, recibe las peticiones de los clientes y las asigna a los workers disponibles. Lleva un historial de que cliente está con qué worker y qué workers están disponibles. El tipo de socket utilizado con clientes y con workers es el de "router".

## worker.js

Es el que hace el procesamiento de los requests de los clientes, lleva un carrito de compra en memoria que se borra cuando el cliente decide terminar su sesión. Sus argumentos son el ID del cliente y la orden con el carrito. Las órdenes que puede recibir son:

* add: añade artículo al carrito de compras

* remove: remueve artículo del carrito de compras

* list: devuelve la lista de artículos que se encuentran en el carrito de compras

* exit: termina la sesión del cliente

  

## client.js

Realiza los requests de los clientes recibiendo como parámetro la orden. Ejemplos de las diferentes órdenes:

* node client.js add:Oreo

* node client.js list

* node client.js add:Pan

* node client.js list

* node client.js remove:Oreo

* node client.js list

* node client.js exit