const zmq = require("zeromq")

async function run() {
  let socket = zmq.socket('req');
  socket.identity = 'Client-'+process.argv[2]

  socket.connect('tcp://localhost:3000')
  console.log("Producer bound to port 3000")

  socket.on('message', (msg) => {
    console.log('resp: ' + msg)
    process.exit(0);
  })

  socket.send(process.argv[3])
  // socket.send('add:Oreos')
}

run()