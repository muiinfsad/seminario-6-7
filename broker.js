const zmq = require('zeromq')
let clients = [],
    requests = [],
    workers = [],
    clients_workers_dictionary = []
let clientsSocket = zmq.socket('router') // frontend
let workersSocket = zmq.socket('router') // backend
clientsSocket.bind('tcp://*:3000')
workersSocket.bind('tcp://*:3001')

console.log('Broker online');


clientsSocket.on('message', (client, sep, message) => {
    client = client.toString()
    message = message.toString()
    console.log(client + '->' + message)

    let search = searchDictionary(clients_workers_dictionary, client)
    if (!search && workers.length > 0) {
        let worker = workers.shift()
        console.log(client + ' and ' + worker + ' are bound');
        clients_workers_dictionary.push({ client: client, worker: worker })
        workersSocket.send([worker, '', client, '', message])
    }
    else if (search) {
        workersSocket.send([clients_workers_dictionary[search].worker, '', client, '', message])
    }
    else if (workers.length == 0) {
        clients.push(client);
        requests.push(message);
        console.log("clients stored");
        console.log(clients);
        console.log("requests stored");
        console.log(requests);
    }

})
workersSocket.on('message', (worker, sep, client, sep2, request) => {
    if (request == 'free') {
        workers.push(worker);
        console.log(worker + ' is free!')
        if (clients.length > 0) {
            let cli = clients.shift()
            let work = workers.shift()
            clients_workers_dictionary.push({ client: cli, worker: work })
            workersSocket.send([work, '', cli, '', requests.shift()])
        }
        return
    }
    else if (request == 'exit') {
        console.log("exit!");
        
        workers.push(worker);
        console.log(worker + '->' + client + ': ok')
        clientsSocket.send([client, '', 'ok'])
        console.log(worker + ' is free!')
        removeFromDictionary(clients_workers_dictionary, client)
        return
    }
    else {
        console.log(worker + '->' + client + ': ' + request)
    }
    clientsSocket.send([client, '', request])
})

function searchDictionary(dictionary, client) {
    for (let d in dictionary) {
        if (dictionary[d].client === client)
            return d
    }
    return false
}

function removeFromDictionary(dictionary, client) {
    let index = searchDictionary(dictionary, client)
    dictionary.splice(index, 1)
}